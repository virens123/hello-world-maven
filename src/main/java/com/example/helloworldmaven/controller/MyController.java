package com.example.helloworldmaven.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {

    @RequestMapping(value = "/test",method = RequestMethod.GET)
    public String helloWorld(){
        return "hello World!!";
    }
}
